﻿# Ce script crée les groupes de domaine local et leur association aux groupes globaux pour mettre en place les partage d'une Entreprise Client.
# Il faudra, à la suite de ce script, aller sur l'interface du Truenas paramétrer les autorisations ad-hoc aux groupes de domaine local.

param(
[array]$entreprises,
[array]$services
)

# On stocke les chemins pour les Distinguished Names dans des variables
$path = "ou=Clients Entreprises,dc=chasseneuil,dc=tierslieux86,dc=fr"
$path_gr_domlocal = "ou=groupes de domaine local,dc=chasseneuil,dc=tierslieux86,dc=fr"

# On parcourt la liste des entreprises donnée en paramètre
foreach ($entreprise in $entreprises)
{
    # On crée les groupes de domaine local qui correspondent au partage général de l'entreprise
    New-ADGroup "partage-$entreprise-READ" -Path $path_gr_domlocal -GroupScope DomainLocal
    New-ADGroup "partage-$entreprise-WRITE" -Path $path_gr_domlocal -GroupScope DomainLocal
    New-ADGroup "partage-$entreprise-FULLCTRL" -Path $path_gr_domlocal -GroupScope DomainLocal
    Add-ADGroupMember -Identity "cn=partage-$entreprise-FULLCTRL,$path_gr_domlocal" -Members "CN=Domain Admins,CN=Users,DC=chasseneuil,DC=tierslieux86,DC=fr"
    
    # On parcourt la liste des services donnée en paramètre
    foreach ($service in $services)
    {
        # On crée les groupes de domaine local qui correspondent au partage de ce service
        New-ADGroup "partage-$service-$entreprise-READ" -Path $path_gr_domlocal -GroupScope DomainLocal
        New-ADGroup "partage-$service-$entreprise-WRITE" -Path $path_gr_domlocal -GroupScope DomainLocal
        New-ADGroup "partage-$service-$entreprise-FULLCTRL" -Path $path_gr_domlocal -GroupScope DomainLocal
        
        # On ajoute les groupes globaux du service aux groupes de domaine local du partage de ce service
        Add-ADGroupMember -Identity "cn=partage-$service-$entreprise-READ,$path_gr_domlocal" -Members "cn=employes-$service-$entreprise,ou=groupes globaux,ou=$entreprise,$path"
        Add-ADGroupMember -Identity "cn=partage-$service-$entreprise-WRITE,$path_gr_domlocal" -Members "cn=responsables-$service-$entreprise,ou=groupes globaux,ou=$entreprise,$path"
        Add-ADGroupMember -Identity "cn=partage-$service-$entreprise-FULLCTRL,$path_gr_domlocal" -Members "CN=Domain Admins,CN=Users,DC=chasseneuil,DC=tierslieux86,DC=fr"
        # On ajoute les groupes globaux du service aux groupes de domaine local du partage général de l'entreprise
        Add-ADGroupMember -Identity "cn=partage-$entreprise-READ,$path_gr_domlocal" -Members "cn=employes-$service-$entreprise,ou=groupes globaux,ou=$entreprise,$path"
        Add-ADGroupMember -Identity "cn=partage-$entreprise-WRITE,$path_gr_domlocal" -Members "cn=responsables-$service-$entreprise,ou=groupes globaux,ou=$entreprise,$path"
    }
}